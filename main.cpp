#include "main.h"



int main(int argc, char *argv[]) {
   QApplication app(argc, argv);
   QCoreApplication::setOrganizationName("Kirby & Co");
   QCoreApplication::setOrganizationDomain("kirbyand.co.uk");
   QCoreApplication::setApplicationName("TTNGateways");
   QCoreApplication::setApplicationVersion("0.1");

   // use QT's portable settings file methods
   QSettings mainSettings;

   // define the parser and what it is looking for from the CL, http://doc.qt.io/qt-5/qcommandlineparser.html
   QCommandLineParser parser;
   parser.setApplicationDescription("A Simple TTN gateways display GUI");
   parser.addHelpOption();
   parser.addVersionOption();

   // parse the invocation cl
   parser.process(app);

   // override/set any relevant settings from invocation cl

   // create the settings defaults if they do not exist already
   SetConfigDefaults();


   // go and do the main appliction now it is configured
   MainWindow w;
   w.show();

   return app.exec();
}


// set the config file defaults for use else where
void SetConfigDefaults() {

   QSettings settings(QCoreApplication::organizationName(), QCoreApplication::applicationName());

   // settings file, recent section
   settings.beginGroup("Recent");
   if(!settings.contains("dbfile")) {
       settings.setValue("dbfile", "default.db");
   }
   settings.endGroup();

   // settigns file, preferences section
   settings.beginGroup("Preferences");
   if(!settings.contains("dbpath")) {
       // do some name mangling to create a default TTNData folder os independantly use Qdir for seperator
       settings.setValue("dbpath", QStandardPaths::locate(QStandardPaths::HomeLocation, QString(), QStandardPaths::LocateDirectory));
   }
    //put stuff here
   settings.endGroup();

   // settigns file, config section
   settings.beginGroup("Config");
   if(!settings.contains("host")) {
       settings.setValue("host", "eu.thethings.network");
   }
   if(!settings.contains("port")) {
       settings.setValue("port", 8883);
   }
   if(!settings.contains("app")) {
       settings.setValue("app", "myapp");
   }
   if(!settings.contains("appkey")) {
       settings.setValue("appkey", "ttn-account-v2.LongRandomString");
   }
   settings.endGroup();

   return;
}
