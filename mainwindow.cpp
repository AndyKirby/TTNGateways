#include "mainwindow.h"

// https://specifications.freedesktop.org/icon-naming-spec/icon-naming-spec-latest.html

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->Status->append("Hello World");

    // do read settings or create default ones
    // do open default database or create default one

}


MainWindow::~MainWindow() {
    delete ui;
}


void MainWindow::on_actionQuit_triggered() {

   // do write and close settings
   // do write and close current database

   QApplication::quit();
}


void MainWindow::on_actionTTN_Application_triggered() {
   ConfigDialog configdialog;
   configdialog.setModal(true);
   configdialog.exec();
}


void MainWindow::on_actionPreferences_triggered() {
    PrefsDialog prefsdialog;
    prefsdialog.setModal(true);
    prefsdialog.exec();
}


void MainWindow::on_actionToolbar_toggled(bool arg1)
{
    if(arg1) {
       ui->toolBar->show();
    } else {
       ui->toolBar->hide();
    }
}


void MainWindow::on_actionGateways_toggled(bool arg1)
{
    if(arg1) {
       ui->FGateways->show();
    } else {
       ui->FGateways->hide();
    }
}


void MainWindow::on_actionNodes_toggled(bool arg1)
{
   if(arg1) {
      ui->FNodes->show();
   } else {
      ui->FNodes->hide();
   }
}


void MainWindow::on_actionStatus_toggled(bool arg1) {
   if(arg1) {
      ui->FStatus->show();
   } else {
      ui->FStatus->hide();
   }
}


void MainWindow::on_actionAbout_triggered()
{
    QMessageBox::about(this, tr("About Application"),
             tr("The <b>Application</b> example demonstrates how to "
                "write modern GUI applications using Qt, with a menu bar, "
                "toolbars, and a status bar."));
}
