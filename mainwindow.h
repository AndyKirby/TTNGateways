#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMessageBox>

#include "ui_mainwindow.h"
#include "configdialog.h"
#include "prefsdialog.h"


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_actionQuit_triggered();
    void on_actionTTN_Application_triggered();
    void on_actionPreferences_triggered();
    void on_actionStatus_toggled(bool arg1);
    void on_actionToolbar_toggled(bool arg1);
    void on_actionGateways_toggled(bool arg1);
    void on_actionNodes_toggled(bool arg1);
    void on_actionAbout_triggered();

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
